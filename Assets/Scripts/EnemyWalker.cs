﻿using UnityEngine;
using System.Collections;

public class EnemyWalker : MonoBehaviour
{
    public float moveSpeed;
    public bool isFacingL;
    public bool isGrounded;

    Rigidbody2D rigidB;
    Animator anim;
    private Character player;


    public Transform groundCheck;
    public LayerMask isGroundLayer;

    int enHealth;

    void Start ()
    {
        rigidB = GetComponent<Rigidbody2D>();

        //Check for Rigidbody
        if (!rigidB)
            Debug.LogError("No Rigidbody 2D found on character");

        //Check for move speed
        //Set movement speed if not set in Inspector.
        if (moveSpeed <= 0)
        {
            moveSpeed = 3.0f;

            Debug.LogWarning("Speed not set in Inspector. Defaulting to " + moveSpeed + ".");
        }

        if (enHealth <=0)
        {
            enHealth = 5;
        }
    }
	
	void Update ()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.2f, isGroundLayer);
        
        float enemyPosition = rigidB.GetComponent<Transform>().position.x;
        float playerPosition = player.GetComponent<Transform>().position.x;



        if (isGrounded == true)
            anim.SetBool("Grounded", true);
        else if (isGrounded == false)
            anim.SetBool("Grounded", false);

        if (enemyPosition > playerPosition && !isFacingL)
        {
            flip();
        }
        else if (enemyPosition < playerPosition && isFacingL)
        {
            flip();
        }
    }

    void OnTriggerEnter2D(Collider2D c)
    {
        // Check if enemy should flip
        if (c.gameObject.tag == "Player")
        {
            // Move enemy in direction it is facing
            if (isFacingL)
            {
                rigidB.velocity = new Vector2(-moveSpeed, rigidB.velocity.y);
            }
            else
            {
                rigidB.velocity = new Vector2(moveSpeed, rigidB.velocity.y);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D a)
    {
        if (a.gameObject.tag == "PlayerProjectile")
        {
            enHealth--;
        }
    }

    void flip()
    {
        isFacingL = !isFacingL;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;
    }

    void jump()
    {
        if (isGrounded == true && GetComponent<Collision2D>().collider.tag == "Player")
        {
            
        }
        else if (isGrounded == false && GetComponent<Collision2D>().collider.tag == "Player")
        {

        }

    }
}
