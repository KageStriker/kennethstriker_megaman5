﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
    public float moveSpeed;
    public float jumpForce;
    public float climbSpeed;
    private float climbVelocity;
    private float gravityStore;

    public bool isGrounded;
    public bool onLadder;
    public bool isFacingL;
    public bool enemyHit;    

    int _lives = 3;
    int playerHealth = 100;

    int score;

    public RectTransform healthBar;

    public Text livesText;

    public Text scoreText;

    Rigidbody2D rigidB;
    Animator anim;
    BoxCollider2D boxCol;

    public LayerMask isGroundLayer;
    public Transform groundCheck;
    
    public Transform projectileSpawn;

    GameObject g;

    public Projectile projectile;

    void Start ()
    {
        boxCol = GetComponent<BoxCollider2D>();
        rigidB = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        gravityStore = rigidB.gravityScale;

        //Check for Rigidbody
        if (!rigidB)
            Debug.LogError("No Rigidbody 2D found on character");

        //Check for a groundCheck
        if (!groundCheck)
            Debug.LogError("No ground check set on character.");

        //Check for move speed
        //Set movement speed if not set in Inspector.
        if (moveSpeed <= 0)
        {
            moveSpeed = 5.0f;

            Debug.LogWarning("Speed not set in Inspector. Defaulting to " + moveSpeed + ".");
        }


        if (jumpForce < 5)
        {
            jumpForce = 5.0f;

            Debug.LogWarning("Jump force not set. Defaulting to " + jumpForce + ".");
        }

        if (Input.GetKey(KeyCode.LeftShift))
        {
            moveSpeed = moveSpeed + 1.0f;
        }
        else
        {
            moveSpeed = 5.0f;
        }

        if (!healthBar)
        {
            Debug.LogError("No health bar linked to player");

            healthBar = GameObject.Find("Health_FG").GetComponent<RectTransform>();
        }

        if (!livesText)
        {
            Debug.LogError("No health bar linked to player");

            livesText = GameObject.Find("livesText").GetComponent<Text>();
        }

        if (!scoreText)
        {
            Debug.LogError("No score linked to player");

            scoreText = GameObject.Find("Score").GetComponent<Text>();
        }



        if (playerHealth <= 0)
            playerHealth = 5;

    }
	
	void Update ()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.2f, isGroundLayer);

        // Used to find out if you are moving or not, and what direction.
        float moveValue = Input.GetAxisRaw("Horizontal");

        rigidB.velocity = new Vector2(moveValue * moveSpeed, rigidB.velocity.y);

        if (isGrounded == true)
            anim.SetBool("Grounded", true);
        else if (isGrounded == false)
            anim.SetBool("Grounded", false);

        if (Input.GetKey(KeyCode.LeftControl))
            anim.SetBool("isAttacking", true);
        else
            anim.SetBool("isAttacking", false);

        if (Input.GetKey(KeyCode.LeftShift))
        {
            anim.SetBool("isSliding", true);
            moveSpeed = 6.0f;
        }
        else
        {
            anim.SetBool("isSliding", false);
            moveSpeed = 5.0f;
        }

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Debug.Log("Jump");

            rigidB.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }
        

        if (onLadder)
        {
            rigidB.gravityScale = 0f;

            climbVelocity = climbSpeed * Input.GetAxisRaw("Vertical");

            rigidB.velocity = new Vector2(rigidB.velocity.x, climbVelocity);
            
            boxCol.size.Set(new_x: 0.5f, new_y:1.5f);

            anim.SetBool("climbIdle", true);
        }

        if (onLadder && climbVelocity > 0 || onLadder && climbVelocity < 0)
        {
            anim.SetBool("isClimbing", true);
            anim.SetBool("climbIdle", false);
        }

        if (!onLadder)
        {
            rigidB.gravityScale = gravityStore;

            anim.SetBool("isClimbing", false);
            anim.SetBool("climbIdle", false);

            boxCol.size.Set(new_x: 1.0f, new_y: 1.5f);
        }

        if (moveValue < 0 && isFacingL)
            flip();
        else if (moveValue > 0 && !isFacingL)
            flip();

        if (Input.GetKeyDown(KeyCode.W) && onLadder)
        {
            Debug.Log("Climb");

            rigidB.AddForce(Vector2.up * climbVelocity, ForceMode2D.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Debug.Log("Shoost");

            Projectile pTemp =
                Instantiate(projectile,
                projectileSpawn.position,
                projectileSpawn.rotation) as Projectile;
            
            if (!isFacingL)
               pTemp.GetComponent<Projectile>().speed *= -1;
        }

        if (!anim)
            Debug.LogError("No Animator found on character");

        if (!projectileSpawn)
            Debug.LogError("No projectileSpawn found on character");

        if (!projectile)
            Debug.LogError("No projectile found on character");
        
        anim.SetFloat("Speed", Mathf.Abs(moveValue));


        if (playerHealth <= 0)
        {
            _lives -= 1;
            Debug.Log("Dead!");
            Destroy(gameObject);
            playerHealth = 100;

            GameManager.instance.spawnPlayer(0);
        }

        if (_lives <= 0)
        {

            GameManager.instance.death = true;
        }
        else
        {
            GameManager.instance.death = false;
        }
        
        healthBar.sizeDelta = new Vector2(playerHealth, healthBar.sizeDelta.y);

        livesText.text = _lives.ToString();
        
    }

    void flip()
    {
        isFacingL = !isFacingL;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;
    }

    // Auto generate get and set methods
    public int lives
    {
        get
        {
            Debug.Log("Getting Lives");
            return _lives;
        }
        set
        {
            Debug.Log("Setting Lives");
            _lives = value;
        }
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == "Enemy" || c.gameObject.tag == "Projectile")
        {
            playerHealth = playerHealth - 20;
        }
    }
}