﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public float speed;
    public float projectileTime;
    public bool isFacingL;

    Rigidbody2D rigidB;

    
    // Use this for initialization
    void Start ()
    {
        float moveValue = Input.GetAxis("Horizontal");

        if (moveValue > 0 && isFacingL )
            flip();
        else if (moveValue < 0 && !isFacingL)
            flip();

        rigidB = GetComponent<Rigidbody2D>();

        GetComponent<Rigidbody2D>().velocity = Vector2.right * speed;

        if (!rigidB)
            Debug.LogError("No Rigidbody 2D found on character");

        
        Destroy(gameObject, projectileTime);
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Level")
        {
            Destroy(gameObject);
        }
    }

    void flip()
    {
        isFacingL = !isFacingL;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;
    }
}

