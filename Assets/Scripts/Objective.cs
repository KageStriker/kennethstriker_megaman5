﻿using UnityEngine;
using System.Collections;

public class Objective : MonoBehaviour
{
    public int rand;
    public int numberOfCollectibles;
    public GameObject[] allCollectibles;

    public GameObject player;
    public GameObject gameFinish;

	void Start ()
    {
        allCollectibles = GameObject.FindGameObjectsWithTag("Collectible");

        numberOfCollectibles = allCollectibles.Length;

        rand = Random.Range(0,2);

        player = GameObject.Find("MegaManSprite");

        gameFinish = GameObject.Find("gameFinish");
    }
	
    void Update()
    {
        if(player && gameFinish)
        {
            float distanceToFinish = Vector2.Distance(
                player.transform.position,
                gameFinish.transform.position);
        }
    }
}
