﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    static GameManager _instance = null;

    public GameObject playerPrefab;
    
    // Use this for initialization
    void Start()
    {
        death = false;

        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            instance = this;

            DontDestroyOnLoad(this); 
        }

        GameObject.FindGameObjectWithTag("Pause");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (SceneManager.GetActiveScene().name == "Title")
            {
                SceneManager.LoadScene(1);
            }
            else if (SceneManager.GetActiveScene().name == "Level1")
            {
                SceneManager.LoadScene(0);
            }
        }
        if (Input.GetKey(KeyCode.R))
        {
            Debug.Log("You pressed the R key");
            if (SceneManager.GetActiveScene().name == "GameOver")
            {
                SceneManager.LoadScene(1);
            }
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log("You pressed the P key");
            if (Time.timeScale == 0)
            {
                GameObject.FindGameObjectWithTag("Pause").GetComponent<Canvas>().enabled = false;
                Time.timeScale = 1;
            }
            else
            {
                GameObject.FindGameObjectWithTag("Pause").GetComponent<Canvas>().enabled = true;
                Time.timeScale = 0;
            }
        }
        /*        if (Input.GetKey(KeyCode.P))
        {
            Debug.Log("You pressed the P key");
            if (SceneManager.GetActiveScene().name == "Level1")
            {
                GameObject.FindGameObjectWithTag("Pause").GetComponent<Canvas>().enabled = true;
                Time.timeScale = 0;
            }
        }
        */

        if (death)
        {
            SceneManager.LoadScene(2);
            death = false;
        }
    }

    public void spawnPlayer(int spawnLocation)
    {
        string spawnPointName = SceneManager.GetActiveScene().name + "_" + spawnLocation;

        Transform spawnPointTransform = GameObject.Find(spawnPointName).GetComponent<Transform>();

        Instantiate(playerPrefab, spawnPointTransform.position, spawnPointTransform.rotation);
    }

    public static GameManager instance
    {
        get { return _instance; }
        set { _instance = value; }
    }
    

    public int score
    {
        get;set;
    }

    public bool death 
    {
        get;set;
    }

    public void startGame()
    {
        Application.LoadLevel("Level1");
    }

    public void quitGame()
    {
        Debug.Log("Quitting Game...");

        Application.Quit();
    }
    
    public void passScreen()
    {
        Application.LoadLevel("PassScreen");
    }

    public void mute()
    {
        Debug.Log("Muted");
    }

    public void unMute()
    {
        Debug.Log("UnMuted");
    }

    public void volume()
    {
        Debug.Log("Volume increased");
    }
}
