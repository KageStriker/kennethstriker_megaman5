﻿using UnityEngine;
using System.Collections;

public class EnemyTurret : MonoBehaviour
{
    private Character player;

    public Transform projectileSpawn;
    public Projectile projectile;
    public Rigidbody2D rb;
    
    public bool isFacingL;
    public bool facingLeft;

    bool shootPlayer;

    int enHealth;
    int points;

    public RectTransform enemyHealthBar;

    public float projectileFireRate;
    float timeSinceLastFire = 0.0f;

    void Start ()
    {
        player = FindObjectOfType<Character>();
        rb = FindObjectOfType<Rigidbody2D>();
        
        if (!projectileSpawn)
            Debug.LogError("No projectileSpawn found on character");

        if (!projectile)
            Debug.LogError("No projectile found on character");

        if (projectileFireRate <= 0)
        {
            projectileFireRate = 2.0f;
        }

        if (enHealth <= 0)
        {
            enHealth = 100;
        }

        if (!enemyHealthBar)
        {
            Debug.LogError("No health bar linked to enemy");

            enemyHealthBar = GameObject.Find("EnHealthFG").GetComponent<RectTransform>();
        }
    }
	
	void Update ()
    {
        float enemyPosition = rb.GetComponent<Transform>().position.x;
        float playerPosition = player.GetComponent<Transform>().position.x;
        //Check that enough time has passed since the last projectile has be fired
        if (shootPlayer == true)
        {
            if (Time.time > timeSinceLastFire + projectileFireRate)
            {
                Projectile pTemp =
                    Instantiate(projectile,
                    projectileSpawn.position,
                    projectileSpawn.rotation) as Projectile;

                if (facingLeft)
                    pTemp.GetComponent<Projectile>().speed *= -1;

                pTemp.GetComponent<Rigidbody2D>().velocity = new Vector2(-5.0f, 0);

                timeSinceLastFire = Time.time;
            }
            if (enemyPosition > playerPosition && !facingLeft)
            {
                flip();
            }
            else if (enemyPosition < playerPosition && facingLeft)
            {
                flip();
            }
        }
        else;

        if (enHealth <= 0)
        {
            Destroy(gameObject);
            points += 100;
        }

        enemyHealthBar.sizeDelta = new Vector2(enHealth, enemyHealthBar.sizeDelta.y);

        player.scoreText.text = "Score: " + points.ToString();
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            shootPlayer = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            shootPlayer = false;
        }
    }

    void OnCollisionEnter2D(Collision2D c)
    {
        if (c.gameObject.tag == "PlayerProjectile")
        {
            points += 10;
            enHealth -= 20;
            Destroy(c.gameObject);
        }
    }

    void flip()
    {
        facingLeft = !facingLeft;

        Vector3 scaleFactor = transform.localScale;

        scaleFactor.x *= -1;

        transform.localScale = scaleFactor;
    }
}
