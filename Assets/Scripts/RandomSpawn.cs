﻿using UnityEngine;
using System.Collections;

public class RandomSpawn : MonoBehaviour {
    public Transform[] allSpawns;
    public GameObject[] allCollectibles;
    // Use this for initialization
    void Start ()
    {
        int randSpawn;
        int randCollect;

        randSpawn = Random.Range(0, 4);
        randCollect = Random.Range(0, 2);
        for (int i = 0; i < 5; ++i)
        {
            randSpawn = Random.Range(0, 4);
            randCollect = Random.Range(0, 2);

            GameObject.Instantiate(allCollectibles[randCollect],
                allSpawns[randSpawn].position,
                allSpawns[randSpawn].rotation);
        }
    }
}