﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour
{

    public Button startButton;
    public Button passButton;

    public Toggle tog;

    public Slider slide;
    

	// Use this for initialization
	void Start ()
    {

        if (SceneManager.GetActiveScene().name == "Title")
        {
            startButton.onClick.AddListener(GameObject.Find("GameManager").GetComponent<GameManager>().startGame);
            passButton.onClick.AddListener(GameObject.Find("GameManager").GetComponent<GameManager>().quitGame);
        }
        else;

        tog.onValueChanged.AddListener(delegate { GameManager.instance.mute(); });
        
        slide.onValueChanged.AddListener(delegate { GameManager.instance.volume(); });
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
