﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]

public class ExtraLife : MonoBehaviour
{
    Rigidbody2D rb;
    BoxCollider2D coll2D;


    void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        coll2D = GetComponent<BoxCollider2D>();

        rb.isKinematic = false;

        rb.constraints = RigidbodyConstraints2D.FreezeRotation;

        rb.gravityScale = 0;

        coll2D.isTrigger = true;
	}

    void OnTriggerEnter2D(Collider2D c)
    {
        if (c.gameObject.tag == "Player")
        {
            Character ch = c.gameObject.GetComponent<Character>();

            if (ch)
            {
                ch.lives++;
            }

            GameManager.instance.score += 10;

            Debug.Log("Score: " + GameManager.instance.score);

            Destroy(gameObject);
        }
    }
}
