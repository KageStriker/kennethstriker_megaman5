﻿using UnityEngine;
using System.Collections;

public class Level1 : MonoBehaviour
{
    public int spawnLocation;

	// Use this for initialization
	void Awake ()
    {
	    if (spawnLocation< 0)
        {
            spawnLocation = 0;
        }

        GameManager.instance.spawnPlayer(spawnLocation);
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
