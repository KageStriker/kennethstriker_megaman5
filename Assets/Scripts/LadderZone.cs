﻿using UnityEngine;
using System.Collections;

public class LadderZone : MonoBehaviour
{

    private Character player;

	void Start ()
    {
        player = FindObjectOfType<Character>();
	}
	
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            player.onLadder = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            player.onLadder = false;
        }
    }
}
